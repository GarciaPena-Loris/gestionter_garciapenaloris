package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Sujet;
import um.fds.agl.ter22.repositories.SujetRepository;

import java.util.Optional;

@Service
public class SujetService {

    @Autowired
    private SujetRepository sujetRepository;

    public Optional<Sujet> getSujet(final Long id) {
        return sujetRepository.findById(id);
    }

    public Iterable<Sujet> getSujets() {
        return sujetRepository.findAll();
    }

    public void deleteSujet(final Long id) {
        sujetRepository.deleteById(id);
    }

    public Sujet saveSujet(Sujet sujet) {
        return sujetRepository.save(sujet);
    }

    public Optional<Sujet> findById(long id) {
        return sujetRepository.findById(id);
    }

}
