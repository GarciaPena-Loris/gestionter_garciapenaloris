package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Groupe;
import um.fds.agl.ter22.entities.Sujet;
import um.fds.agl.ter22.repositories.GroupeRepository;

import java.util.Optional;

@Service
public class GroupeService {
    @Autowired
    private GroupeRepository groupeRepository;

    public Optional<Groupe> getGroupe(final Long id) {
        return groupeRepository.findById(id);
    }

    public Iterable<Groupe> getGroupes() {
        return groupeRepository.findAll();
    }

    public void deleteGroupe(final Long id) {
        groupeRepository.deleteById(id);
    }

    public Groupe saveGroupe(Groupe groupe) {
        Groupe savedGroupe = groupeRepository.save(groupe);
        return savedGroupe;
    }

    public Optional<Groupe> findById(long id) {
        return groupeRepository.findById(id);
    }

}
