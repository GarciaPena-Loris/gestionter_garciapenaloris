package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.Student;

import java.util.ArrayList;

public class GroupeForm {
    private long id;
    private String nom;

    private ArrayList<Student> listeEtudiants = new ArrayList<>();


    public GroupeForm(){}

    public GroupeForm(long id, String nom) {
        this.nom = nom;
        this.id = id;
    }

    public GroupeForm(long id, String nom, ArrayList<Student> listeEtudiants) {
        this.id = id;
        this.nom = nom;
        this.listeEtudiants = listeEtudiants;
    }

    public String getNom(){ return nom; }
    public void setNom(String nom){this.nom = nom; }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<Student> getListeEtudiants() {
        return listeEtudiants;
    }

    public void setListeEtudiant(ArrayList<Student> listeEtudiants) {
        this.listeEtudiants = listeEtudiants;
    }

}