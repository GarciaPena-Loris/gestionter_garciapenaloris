package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.Teacher;

import java.util.ArrayList;
import java.util.List;

public class SujetForm {
    private String nomEnseignantResponsable;
    private List<Teacher> listeEnseignantsSecondaires;
    private String titre;
    private long id;

    public SujetForm() {}

    public SujetForm(long id, String titre, String nomEnseignantResponsable, List<Teacher> listeEnseignantsSecondaires) {
        this.nomEnseignantResponsable = nomEnseignantResponsable;
        this.listeEnseignantsSecondaires = listeEnseignantsSecondaires;
        this.titre = titre;
        this.id = id;
    }

    public SujetForm(long id, String titre, String nomEnseignantResponsable) {
        this.nomEnseignantResponsable = nomEnseignantResponsable;
        this.titre = titre;
        this.id = id;
    }
    public void setNomEnseignantResponsable(String nomEnseignantResponsable) {
        this.nomEnseignantResponsable = nomEnseignantResponsable;
    }

    public String getNomEnseignantResponsable() {
        return nomEnseignantResponsable;
    }

    public void setListeEnseignantsSecondaires(ArrayList<Teacher> listeEnseignantsSecondaires) {
        this.listeEnseignantsSecondaires = listeEnseignantsSecondaires;
    }

    public List<Teacher> getListeEnseignantsSecondaires() {
        return listeEnseignantsSecondaires;
    }

    public int getNombreEnseignantSecondaire() {
        if (listeEnseignantsSecondaires.isEmpty())
            return 0;
        else
            return listeEnseignantsSecondaires.size();
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SujetForm{" +
                "nomEnseignantResponsable='" + nomEnseignantResponsable + '\'' +
                ", listeEnseignantsSecondaires=" + listeEnseignantsSecondaires +
                ", titre='" + titre + '\'' +
                ", id=" + id +
                '}';
    }
}
