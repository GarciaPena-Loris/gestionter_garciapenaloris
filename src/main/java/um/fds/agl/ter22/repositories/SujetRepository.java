package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Sujet;

public interface SujetRepository
        extends CrudRepository<Sujet, Long> {

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_TEACHER') or #sujet?.enseignantResponsable.lastName == authentication?.name")
    Sujet save(@Param("sujet") Sujet sujet);

    @PreAuthorize("hasRole('ROLE_MANAGER') or @sujetRepository.findById(#id).get()?.enseignantResponsable.lastName == authentication?.name")
    void deleteById(@Param("id") Long id);

    @PreAuthorize("hasRole('ROLE_MANAGER') or #sujet?.enseignantResponsable.lastName == authentication?.name")
    void delete(@Param("sujet") Sujet sujet);

}
