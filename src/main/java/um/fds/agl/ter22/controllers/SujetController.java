package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Sujet;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.forms.SujetForm;
import um.fds.agl.ter22.services.SujetService;
import um.fds.agl.ter22.services.TeacherService;

import java.util.ArrayList;

@Controller
public class SujetController implements ErrorController {

    @Autowired
    private SujetService sujetService;

    @Autowired
    private TeacherService teacherService;

    @GetMapping("/listSujets")
    public Iterable<Sujet> getSujets(Model model) {
        model.addAttribute("sujets", sujetService.getSujets());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Boolean estManager = authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_MANAGER"));
        Boolean estTeacher = authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_TEACHER"));
        model.addAttribute("estManager", estManager);
        model.addAttribute("estTeacher", estTeacher);

        model.addAttribute("nomActuel", authentication.getName());


        return sujetService.getSujets();
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_TEACHER')")
    @GetMapping(value = {"/addSujet"})
    public String showAddSujetPage(Model model) {

        SujetForm sujetForm = new SujetForm();
        model.addAttribute("sujetForm", sujetForm);

        return "addSujet";
    }

    @PostMapping(value = {"/addSujet"})
    public String addSujets(Model model, @ModelAttribute("SujetForm") SujetForm sujetForm) {
        Sujet s;
        Teacher newTeacherPrincipal = teacherService.findByLastName(sujetForm.getNomEnseignantResponsable());
        if (newTeacherPrincipal == null) {
            System.err.println("Aucun enseignant ne correspond à ce nom !");
            model.addAttribute("errorMessage", "Aucun enseignant ne correspond au nom : " + sujetForm.getNomEnseignantResponsable());
            return "error";
        }
        else {
            if (sujetService.findById(sujetForm.getId()).isPresent()) {

                // sujet already existing : update
                s = sujetService.findById(sujetForm.getId()).get();
                s.setTitre(sujetForm.getTitre());
                s.setEnseignantResponsable(newTeacherPrincipal);
            } else {
                // sujet not existing : create
                s = new Sujet(newTeacherPrincipal, sujetForm.getTitre());
            }
            sujetService.saveSujet(s);
        }
        return "redirect:/listSujets";

    }

    @GetMapping(value = {"/showSujetUpdateForm/{id}"})
    public String showSujetUpdateForm(Model model, @PathVariable(value = "id") long id) {
        SujetForm sujetForm = new SujetForm(id, sujetService.findById(id).get().getTitre(), sujetService.findById(id).get().getEnseignantResponsable().getLastName(), sujetService.findById(id).get().getListeEnseignantsSecondaires());
        model.addAttribute("sujetForm", sujetForm);

        return "updateSujet";
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or @sujetRepository.findById(#id).get()?.enseignantResponsable.lastName == authentication?.name")
    @GetMapping(value = {"/deleteEnseignantSecondaire/{id}/{nomEnseignant}"})
    public String deleteEnseignantSecondaire(Model model, @PathVariable(value = "id") long id, @PathVariable(value = "nomEnseignant") String nomEnseignant) {
        Sujet s = sujetService.findById(id).get();
        s.delEnseignantSecondaire(teacherService.findByLastName(nomEnseignant));
        sujetService.saveSujet(s);

        return "redirect:/showSujetUpdateForm/" + id;
    }

    @GetMapping(value = {"/addEnseignantSecondaire/{id}"})
    public String ajouterEnseignantSecondaire(Model model, @PathVariable(value = "id") long id) {
        SujetForm sujetFormAddEnseignant = new SujetForm(id, sujetService.findById(id).get().getTitre(), sujetService.findById(id).get().getEnseignantResponsable().getLastName(), sujetService.findById(id).get().getListeEnseignantsSecondaires());
        model.addAttribute("sujetFormAddEnseignant", sujetFormAddEnseignant);

        Iterable<Teacher> teachers=teacherService.getTeachers();
        ArrayList<Teacher> teachersWithoutActualTeacher = new ArrayList<>();

        teachers.forEach(teacher -> {
            if (!sujetFormAddEnseignant.getListeEnseignantsSecondaires().contains(teacher) && teacher.getLastName() != sujetFormAddEnseignant.getNomEnseignantResponsable()) {
                teachersWithoutActualTeacher.add(teacher);
            }
        });
        model.addAttribute("teachersWithoutActualTeacher", teachersWithoutActualTeacher.iterator());

        return "listeAddEnseignant";
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or @sujetRepository.findById(#id).get()?.enseignantResponsable.lastName == authentication?.name")
    @GetMapping(value = {"/addTeacherToSujet/{id}/{nomEnseignant}"})
    public String addTeacherToSujet(Model model, @PathVariable(value = "id") long id, @PathVariable(value = "nomEnseignant") String nomEnseignant) {
        Sujet s = sujetService.findById(id).get();
        s.addEnseignantSecondaire(teacherService.findByLastName(nomEnseignant));
        sujetService.saveSujet(s);

        return "redirect:/showSujetUpdateForm/" + id;
    }

    @GetMapping(value = {"/deleteSujet/{id}"})
    public String deleteSujet(Model model, @PathVariable(value = "id") long id) {
        sujetService.deleteSujet(id);
        return "redirect:/listSujets";
    }
}
