package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.SecurityConfiguration;
import um.fds.agl.ter22.entities.Groupe;
import um.fds.agl.ter22.entities.Sujet;
import um.fds.agl.ter22.entities.UserTER;
import um.fds.agl.ter22.forms.GroupeForm;
import um.fds.agl.ter22.forms.SujetForm;
import um.fds.agl.ter22.services.GroupeService;
import um.fds.agl.ter22.services.StudentService;

@Controller
public class GroupeController implements ErrorController {
    @Autowired
    private GroupeService groupeService;

    @Autowired
    private StudentService studentService;

    @GetMapping("/listGroupes")
    public Iterable<Groupe> getGroupes(Model model) {
        model.addAttribute("groupes", groupeService.getGroupes());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("student", studentService.findByLastName(authentication.getName()));

        Boolean estManager = authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_MANAGER"));
        model.addAttribute("estManager", estManager);

        Boolean estStudent = authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_STUDENT"));
        model.addAttribute("estStudent", estStudent);

        return groupeService.getGroupes();
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    @GetMapping(value = {"/addGroupe"})
    public String showAddGroupePage(Model model) {

        GroupeForm groupeForm = new GroupeForm();
        model.addAttribute("groupeForm", groupeForm);

        return "addGroupe";
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    @PostMapping(value = {"/addGroupe"})
    public String addGroupes(Model model, @ModelAttribute("GroupeForm") GroupeForm groupeForm) {
        Groupe g;
        if (groupeService.findById(groupeForm.getId()).isPresent()) {
            g = groupeService.findById(groupeForm.getId()).get();
            g.setNom(groupeForm.getNom());
        } else {
            g = new Groupe(groupeForm.getNom());
        }
        groupeService.saveGroupe(g);
        return "redirect:/listGroupes";

    }

    @GetMapping(value = {"/showGroupeUpdateForm/{id}"})
    public String showGroupeUpdateForm(Model model, @PathVariable(value = "id") long id) {

        GroupeForm groupeForm = new GroupeForm(id, groupeService.findById(id).get().getNom());
        model.addAttribute("groupeForm", groupeForm);
        return "updateGroupe";
    }

    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @GetMapping(value = {"/addStudentToGroup/{id}/{nomStudent}"})
    public String addStudentToGroup(Model model, @PathVariable(value = "id") long id, @PathVariable(value = "nomStudent") String nomStudent) {
        Groupe g = groupeService.findById(id).get();
        g.addEtudiant(studentService.findByLastName(nomStudent));
        groupeService.saveGroupe(g);

        return "redirect:/listGroupes/";
    }

    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @GetMapping(value = {"/delStudentToGroup/{id}/{nomStudent}"})
    public String delStudentToGroup(Model model, @PathVariable(value = "id") long id, @PathVariable(value = "nomStudent") String nomStudent) {

        Groupe g = groupeService.findById(id).get();
        g.delEtudiant(studentService.findByLastName(nomStudent));
        groupeService.saveGroupe(g);

        return "redirect:/listGroupes/";
    }


    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    @GetMapping(value = {"/deleteGroupe/{id}"})
    public String deleteGroupe(Model model, @PathVariable(value = "id") long id) {
        groupeService.deleteGroupe(id);
        return "redirect:/listGroupes";
    }

}
