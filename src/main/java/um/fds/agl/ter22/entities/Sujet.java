package um.fds.agl.ter22.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Inheritance
public class Sujet {
    private @Id @GeneratedValue Long id;

    @ManyToOne
    @JoinTable(name = "sujet_teacher_principal",
            joinColumns = @JoinColumn(name = "sujet_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private Teacher enseignantResponsable;

    @ManyToMany
    @JoinTable(name = "sujet_teacher_secondaire",
            joinColumns = @JoinColumn(name = "sujet_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    public List<Teacher> listeEnseignantsSecondaires = new ArrayList<>();
    private String titre;

    public Sujet() {
    }

    public Sujet(Teacher enseignantResponsable, String titre) {
        this.enseignantResponsable = enseignantResponsable;
        this.titre = titre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Teacher getEnseignantResponsable() {
        return enseignantResponsable;
    }

    public List<Teacher> getListeEnseignantsSecondaires() {
        return listeEnseignantsSecondaires;
    }

    public void setEnseignantResponsable(Teacher enseignantResponsable) {
        this.enseignantResponsable = enseignantResponsable;
    }


    public void addEnseignantSecondaire(Teacher enseignantSecondaire) {
        this.listeEnseignantsSecondaires.add(enseignantSecondaire);
    }

    public void delEnseignantSecondaire(Teacher enseignantSecondaire) {
        this.listeEnseignantsSecondaires.remove(enseignantSecondaire);
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sujet)) return false;
        Sujet sujet = (Sujet) o;
        return Objects.equals(getId(), sujet.getId()) &&
                Objects.equals(getEnseignantResponsable(), sujet.getEnseignantResponsable()) &&
                Objects.equals(getListeEnseignantsSecondaires(), sujet.getListeEnseignantsSecondaires()) &&
                Objects.equals(getTitre(), sujet.getTitre());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getEnseignantResponsable().hashCode();
        result = 31 * result + getListeEnseignantsSecondaires().hashCode();
        result = 31 * result + getTitre().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Sujet{" +
                "id=" + id +
                ", nomEnseignantResponsable='" + enseignantResponsable + '\'' +
                ", listeEnseignantsSecondaires=" + listeEnseignantsSecondaires +
                ", titre='" + titre + '\'' +
                '}';
    }
}
