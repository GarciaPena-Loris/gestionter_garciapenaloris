package um.fds.agl.ter22.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance
public class Groupe {
    private @Id @GeneratedValue Long id;
    private String nom;

    @OneToMany
    @JoinTable(name = "groupe_student",
            joinColumns = @JoinColumn(name = "groupe_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id"))
    private List<Student> listeEtudiants = new ArrayList<>();

    public Groupe() {}

    public Groupe(String nom) {
        this.nom = nom;
    }

    public Groupe(String nom, ArrayList<Student> listeEtudiants) {
        this.nom = nom;
        this.listeEtudiants = listeEtudiants;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Student> getListeEtudiants() {
        return listeEtudiants;
    }

    public void setListeEtudiant(ArrayList<Student> listeEtudiants) {
        this.listeEtudiants = listeEtudiants;
    }

    public void addEtudiant(Student student) {
        listeEtudiants.add(student);
    }

    public void delEtudiant(Student student) {
        listeEtudiants.remove(student);
    }

    @Override
    public String toString() {
        return "Groupe{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", listeEtudiants=" + listeEtudiants +
                '}';
    }
}
