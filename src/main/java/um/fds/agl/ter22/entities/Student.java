package um.fds.agl.ter22.entities;

import javax.persistence.*;

@Entity
@Inheritance
public class Student extends UserTER{

    @ManyToOne
    @JoinTable(name = "groupe_student",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "groupe_id"))
    private Groupe groupe;

    public Groupe getGroupe() {
        return groupe;
    }

    public void setGroupe(Groupe groupe) {
        this.groupe = groupe;
    }

    public Student(String firstName, String lastName){
        super(firstName, lastName);
        String[] roles={"ROLE_STUDENT"};
        this.setRoles(roles);
    }

    public Student(long id, String firstName, String lastName) {
        super(id, firstName, lastName);
        String[] roles = {"ROLE_STUDENT"};
        this.setRoles(roles);
    }
    public Student() {}

    @Override
    public String toString() {
        return "Student{" +
                " prenom : " + getFirstName() +
                " nom: " + getLastName() +
                '}';
    }
}
