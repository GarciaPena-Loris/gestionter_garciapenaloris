package um.fds.agl.ter22;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import um.fds.agl.ter22.entities.*;
import um.fds.agl.ter22.repositories.*;

import java.util.ArrayList;
import java.util.Arrays;

@Component
public class DatabaseLoader implements CommandLineRunner {
    private final TeacherRepository teachers;
    private final TERManagerRepository managers;

    private final StudentRepository students;
    private final SujetRepository sujets;
    private final GroupeRepository groupes;



    @Autowired
    public DatabaseLoader(TeacherRepository teachers, TERManagerRepository managers, StudentRepository students, SujetRepository sujets, GroupeRepository groupes) {
        this.teachers = teachers;
        this.managers=managers;
        this.students=students;
        this.sujets=sujets;
        this.groupes=groupes;
    }

    @Override
    public void run(String... strings) throws Exception {
        TERManager terM1Manager=this.managers.save(new TERManager("Le","Chef", "mdp", "ROLE_MANAGER"));
       SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("Chef", "bigre",
                        AuthorityUtils.createAuthorityList("ROLE_MANAGER"))); // the actual password is not needed here
        this.teachers.save(new Teacher("Ada", "Lovelace", "lovelace",terM1Manager, "ROLE_TEACHER"));
        this.teachers.save(new Teacher("Alan", "Turing", "turing",terM1Manager, "ROLE_TEACHER"));
        this.teachers.save(new Teacher("Leslie", "Lamport", "lamport",terM1Manager, "ROLE_TEACHER"));
        this.teachers.save(new Teacher("Michel", "Logique", "logique",terM1Manager, "ROLE_TEACHER"));
        this.teachers.save(new Teacher("Mandarine", "Nebut", "nebut",terM1Manager, "ROLE_TEACHER"));
        this.students.save(new Student("Gustave", "Flaubert"));
        this.students.save(new Student("Frédéric", "Chopin"));
        this.students.save(new Student("Wolfgang Amadeus", "Mozart"));
        this.students.save(new Student("Jean-Sebastien", "Bach"));
        this.students.save(new Student("Clause", "Debussy"));
        this.students.save(new Student("Angus", "Young"));
        this.students.save(new Student("Loris", "Garcia-Pena"));
        this.students.save(new Student("Elliot", "Mazerand"));
        this.students.save(new Student("Paulo", "leSeulTout"));
        Groupe dracoFire = new Groupe("DracoFire");
        dracoFire.addEtudiant(students.findByLastName("Garcia-Pena"));
        dracoFire.addEtudiant(students.findByLastName("Mazerand"));
        Groupe informaticiens = new Groupe("Les informaticiens");
        Groupe artistes = new Groupe("Les artistes");
        artistes.addEtudiant(students.findByLastName("Flaubert"));
        artistes.addEtudiant(students.findByLastName("Chopin"));
        artistes.addEtudiant(students.findByLastName("Mozart"));
        artistes.addEtudiant(students.findByLastName("Bach"));
        artistes.addEtudiant(students.findByLastName("Debussy"));
        Groupe ACDC = new Groupe("ACDC");
        ACDC.addEtudiant(students.findByLastName("Young"));
        this.groupes.save(dracoFire);
        this.groupes.save(informaticiens);
        this.groupes.save(artistes);
        this.groupes.save(ACDC);

        Sujet elec = new Sujet(teachers.findByLastName("Turing"), "Les joies de l'élécriticté !");
        elec.addEnseignantSecondaire(teachers.findByLastName("Lamport"));
        elec.addEnseignantSecondaire(teachers.findByLastName("Lovelace"));
        this.sujets.save(elec);

        Sujet bonheur = new Sujet(teachers.findByLastName("Lovelace"), "Que du bonheur...");
        bonheur.addEnseignantSecondaire(teachers.findByLastName("Nebut"));
        this.sujets.save(bonheur);

        Sujet arit = new Sujet(teachers.findByLastName("Turing"), "Apprendre l'arithmetique");
        arit.addEnseignantSecondaire(teachers.findByLastName("Nebut"));
        arit.addEnseignantSecondaire(teachers.findByLastName("Lovelace"));
        this.sujets.save(arit);

        Sujet mdc = new Sujet(teachers.findByLastName("Logique"), "Comprendre modeles de calculs");
        this.sujets.save(mdc);


        SecurityContextHolder.clearContext();

    }
}
