package seleniumTests;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class StretchTest extends BaseForTests {

    @Test
    void ajoutEnseignantFromEnseignantError() throws NoSuchElementException {
        login("Garcia-Pena", "Garcia-Pena");
        driver.findElement(By.linkText("Teachers List")).click();
        assertThrows(NoSuchElementException.class, () -> {
            driver.findElement(By.linkText("Add Teachers"));
        });
    }

    @Test
    void ajoutEnseignantFromManager() throws IOException {
        login("Chef", "mdp");
        driver.findElement(By.linkText("Teachers List")).click();
        driver.findElement(By.linkText("Add Teachers")).click();
        write(driver.findElement(By.id("firstName")), "TeacherfisrtnameforTest");
        write(driver.findElement(By.id("lastName")), "TeacherlastnameforTest");
        driver.findElement(By.cssSelector("[type=submit]")).click();

        assertTrue(driver.getTitle().contains("Teacher List"));
        WebElement body = driver.findElement(By.tagName("body"));
        assertTrue(body.getText().contains("TeacherfisrtnameforTest"));
        assertTrue(body.getText().contains("TeacherlastnameforTest"));
        screenshot("PHOTO");
    }
}
