package um.fds.agl.ter22.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.services.TeacherService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;

    //Pour test JUnit :
    @Autowired
    TeacherService teacherService;

    //Pour Test mock
//    @MockBean
//    TeacherService teacherService;


    @Captor
    ArgumentCaptor<Teacher> captorTeacher;

//    @Test
//    @WithMockUser(username = "Chef", roles = "MANAGER")
//    void addTeacherPostNonExistingTeacherWithMock() throws Exception {
//        when(teacherService.findById(10)).thenReturn(Optional.ofNullable(null));
//
//        mvc.perform(post("/addTeacher")
//                .param("firstName", "Thomas")
//                .param("lastName", "Toketchup")
//                .param("id", "10"))
//        .andExpect(status().is3xxRedirection())
//        .andReturn();
//
//        verify(teacherService, atLeastOnce()).saveTeacher(captorTeacher.capture());
//        Teacher capturedTeacher = captorTeacher.getValue();
//        assertEquals("Thomas", capturedTeacher.getFirstName());
//
//    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        assumingThat(teacherService.findByLastName("Kermarrec") == null,
                () -> {
                    MvcResult result = mvc.perform(post("/addTeacher")
                                    .param("firstName", "Anne-Marie")
                                    .param("lastName", "Kermarrec")
                                    .param("id", "10")
                            )
                            .andExpect(status().is3xxRedirection())
                            .andReturn();
                    //Utilisation impossible car l'id est généré automatiquement
                    //assertTrue(teacherService.getTeacher(10l).isPresent());
                    assertEquals(teacherService.findByLastName("Kermarrec").getFirstName(), "Anne-Marie");
                });
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception {
        //ajout un albert einstein a la liste des enseignant
        mvc.perform(post("/addTeacher")
                .param("firstName", "Albert")
                .param("lastName", "Einstein"));
        //verifie qu'il est bien été ajouté
        assumingThat(teacherService.findByLastName("Einstein") != null,
                () -> {
                    //recupere l'id
                    long id = teacherService.findByLastName("Einstein").getId();

                    //verifie que le nom est bien Alvert
                    assertEquals(teacherService.findById(id).get().getFirstName(), "Albert");

                    //Ajout un teacher avec cette meme id
                    MvcResult result = mvc.perform(post("/addTeacher")
                                    .param("firstName", "Alberto")
                                    .param("lastName", "Einstein")
                                    .param("id", Long.toString(id))
                            )
                            .andExpect(status().is3xxRedirection())
                            .andReturn();
                    //verifie que le nom a bien été changé en Alberto
                    assertEquals(teacherService.findById(id).get().getFirstName(), "Alberto");
                });
    }


    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherGet() throws Exception {
        mvc.perform(get("/addTeacher"))
            .andExpect(status().isOk())
            .andExpect(content().contentType("text/html;charset=UTF-8"))
            .andExpect(view().name("addTeacher"))
            .andReturn();
    }
}